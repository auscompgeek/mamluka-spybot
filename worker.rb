require 'sidekiq'
require 'elasticsearch'
require 'settingslogic'

class Settings < Settingslogic
  source "./config.yml"
end

Sidekiq.configure_server do |config|
  config.redis = {url: "redis://#{Settings.redis}:6379/12"}
end

Sidekiq.configure_client do |config|
  config.redis = {url: "redis://#{Settings.redis}:6379/12"}
end


$client = Elasticsearch::Client.new log: true

class IndexIrcChat
  include Sidekiq::Worker

  def perform(network, channel, nick, message, time)
    time = Time.at(time)

    id = Digest::SHA2.hexdigest(network + channel + nick + message)[0..19]

    $client.index index: 'irc', type: 'message', id: id, body: {
                                  network: network,
                                  channel: channel,
                                  nick: nick,
                                  message: message,
                                  simple_message: message,
                                  time: time,
                                  date: time.strftime('%Y-%m-%d'),
                                  iso_time: time.iso8601
                              }
  end
end


