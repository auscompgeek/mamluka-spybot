require 'elasticsearch'
require 'json'

$client = Elasticsearch::Client.new log: false, host: '54.68.252.146'

start_time = Time.now
docs = $client.search index: 'irc', body: {
                                      query: {
                                          range: {
                                              date: {
                                                  gte: (Time.now-3600*24).strftime('%Y-%m-%d'),
                                                  lte: "now"
                                              }
                                          }
                                      },
                                      size: 10**6, sort: {iso_time: {order: "asc"}}
                                  }
p "Index took #{Time.now - start_time}"

docs = docs['hits']['hits'].map.with_index { |x, i| s = x['_source']; s['id'] = i; s }

why_docs = docs.select { |x| x['message'].scan(/^(what|where|who|should i|can i|why|how|can you)\s/).any? && x['message'][-1..-1] != '?' && x['message'].length > 15 }

File.write 'why.json', JSON.pretty_generate(why_docs.map { |x| x['message'] })