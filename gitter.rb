require 'eventmachine'
require 'em-http'
require 'json'
require 'thor'
require 'rest-client'
require 'peach'

require_relative 'worker'

class Gitter < Thor
  desc 'listen', 'listen'

  def listen(room_file, block, size)

    rooms = JSON.parse(File.read(room_file))
    token = 'adfff1a5d148c8125fd80ed4cb6a8aa61a20838c'

    block ||= 0
    start_pos = size.to_i*block.to_i
    end_pos = start_pos+size.to_i-1

    all_http = rooms.uniq { |x| x['id'] }.sort_by { |x| x['userCount'].to_i }.reverse[start_pos..end_pos].map { |x|
      stream_url = "https://stream.gitter.im/v1/rooms/#{x['id']}/chatMessages"
      {room: x['name'].gsub('/', '-'), http: EM::HttpRequest.new(stream_url, keepalive: true, connect_timeout: 0, inactivity_timeout: 0)}
    }

    EventMachine.run do
      all_http.each { |x|
        req = x[:http].get(head: {'Authorization' => "Bearer #{token}", 'accept' => 'application/json'})

        req.stream do |chunk|
          unless chunk.strip.empty?
            begin
              message = JSON.parse(chunk)
              time = Time.now.utc.to_i
              # p message
              IndexIrcChat.perform_async 'gitter', x[:room], message['fromUser']['username'], message['text'], time
            rescue Exception => ex
              p ex.message
            end
          end
        end
      }

    end
  end

  desc 'crawl', 'crawl'

  def crawl(start, pages)
    (start.to_i..pages.to_i).map { |x|
      response = RestClient.get "https://github.com/search?p=#{x}&q=stars%3A%3E1&s=stars&type=Repositories", user_agent: 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'

      sleep 10

      html = Nokogiri::HTML response
      html.css('.repo-list-name a').map { |x| x.content }

    }.flatten.each { |x| $stdout.puts x }
  end

  desc 'collect_rooms', 'collect_rooms'

  def collect_rooms(filename)
    rooms = File.read(filename).split("\n")

    $stdout.puts JSON.pretty_generate(Hash[rooms.pmap { |x|
                                             begin
                                               response = RestClient.get "https://gitter.im/#{x}/~chat"

                                               [x, response.scan(/id":"(.*?)"/).first.first]
                                             rescue Exception => ex
                                               # p ex.message
                                               nil
                                             end
                                           }.compact])

  end
end

Gitter.start