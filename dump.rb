require 'elasticsearch'
require 'json'
require 'thor'

class Dump < Thor
  desc 'dump', 'dump'

  def dump(host, pages, size)
    size = size.to_i
    pages = pages.to_i

    $client = Elasticsearch::Client.new log: false, host: host

    (0..pages).map { |x|
      start_time = Time.now
      index = $client.search index: 'irc', body: {
                                             query: {
                                                 match_all: {}
                                             },
                                             from: size*x,
                                             size: size
                                         }
      p "Index took #{Time.now - start_time}"

      File.write "docs-#{x}.json", JSON.generate(index['hits']['hits'])
    }

  end
end

Dump.start
