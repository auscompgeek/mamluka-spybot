require 'elasticsearch'
require 'json'

$client = Elasticsearch::Client.new log: false, host: '54.68.252.146'

start_time = Time.now
docs = $client.search index: 'irc', body: {
                                      query: {
                                          range: {
                                              date: {
                                                  gte: (Time.now-3600*24).strftime('%Y-%m-%d'),
                                                  lte: "now"
                                              }
                                          }
                                      },
                                      size: 10**6, sort: {iso_time: {order: "asc"}}
                                  }
p "Index took #{Time.now - start_time}"

docs = docs['hits']['hits'].map.with_index { |x, i| s = x['_source']; s['id'] = i; s }

ebay_docs = docs.select { |x| x['message'].include?('www.ebay.com') || x['message'].include?('www.amazon.com') }

File.write 'ebay.json', JSON.pretty_generate(ebay_docs.map { |x|
                                                 # iso_time = Time.parse(x[' iso_time '])
                                                 #
                                                 # messages = docs.select { |doc| Time.parse(doc[' iso_time ']) > (iso_time - 60*10) && Time.parse(doc[' iso_time ']) < (iso_time + 60*10) }.map { |x| x[' message '] }

                                                 messages = docs.select { |doc| doc['id'] > x['id'] - 5 && doc['id'] < x['id'] + 10 }.map { |x| x['message'] }

                                                 {
                                                     ebay: x['message'],
                                                     messages: messages
                                                 }
                                               })