require 'elasticsearch'

$client = Elasticsearch::Client.new log: false, host: '54.68.252.146'

start_time = Time.now
docs = $client.search index: 'irc', body: {query: {match_all: {}}, size: 10**6, sort: {iso_time: {order: "asc"}}}
p "Index took #{Time.now - start_time}"

number_of_questions = docs['hits']['hits'].map { |x| x['_source'] }.group_by { |x| x['channel'] + x['date'] }.map { |k, v|
  questions = {}
  v.each { |x|
    if x['message'][-1..-1] == "?"
      if questions[x['nick']]
        questions[x['nick']][:messages] << x['message']
      else
        questions[x['nick']] = {
            messages: [x['message']],
            channel: x['channel'],
            nick: x['nick'],
            date: x['date']
        }
      end
    end

    reply_nick = x['message'].scan(/^(.*?):/).first.first rescue nil
    if reply_nick && questions[reply_nick]
      questions[reply_nick][:messages] << x['message']
    end
  }

  questions.select { |k, v| v[:messages].length > 1 }.values
}.flatten.compact.length

p "Number of questions #{number_of_questions}"

number_of_long_youtube = docs['hits']['hits'].map { |x| x['_source'] }.select { |x|
  x['message'].include?('youtu') && x['message'].length > 93
}
p "Number of long you tube #{number_of_long_youtube.length}"

number_of_long_jpg = docs['hits']['hits'].map { |x| x['_source'] }.select { |x|
  x['message'].include?('.jpg') && x['message'].include?('http') && x['message'].length - x['message'].scan(/http.*?\s|http.*?$/).first.length > 35
}.length
p "Number of long jpg #{number_of_long_jpg}"

number_of_long_gif = docs['hits']['hits'].map { |x| x['_source'] }.select { |x|
  x['message'].include?('.gif') && x['message'].include?('http') && x['message'].length - x['message'].scan(/http.*?\s|http.*?$/).first.length > 15
}.length
p "Number of long gif #{number_of_long_gif}"

number_of_links_with_answers = docs['hits']['hits'].map { |x| x['_source'] }.group_by { |x| x['channel'] }.map { |k, v|
  questions = {}
  v.each { |x|
    if x['message'].include?("http")
      if questions[x['nick']]
        questions[x['nick']][:messages] << x['message']
      else
        questions[x['nick']] = {
            messages: [x['message']],
            channel: x['channel'],
            nick: x['nick'],
            date: x['date']
        }
      end
    end

    reply_nick = x['message'].scan(/^(.*?):/).first.first rescue nil
    if reply_nick && questions[reply_nick]
      questions[reply_nick][:messages] << x['message']
    end
  }

  questions.select { |k, v| v[:messages].length > 1 }.values
}.flatten.compact.each { |x| $stdout.puts ['------'] + x[:messages] + ['--------'] }.length

p "Number of links with answers #{number_of_links_with_answers}"





